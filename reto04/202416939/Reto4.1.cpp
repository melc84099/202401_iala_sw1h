#include <iostream>
#include <cstdlib> 
#include <ctime>
using namespace std;

int* ArregloAleatorio(int& tamano) {
    tamano = rand() % 401 + 100;
    int* arreglo = new int[tamano];
    for (int i = 0; i < tamano; ++i) {
        arreglo[i] = rand() % 10000 + 1;
    }
    return arreglo;
}
int main() {

    srand(time(NULL));
    int tamano;
    int* arreglo = ArregloAleatorio(tamano);
    cout << "Tama�o del arreglo: " << tamano << endl;
    cout << "Arreglo: ";
    for (int i = 0; i < tamano; ++i) {
        cout << arreglo[i] << " ";
    }
    cout << endl;
    delete[] arreglo;

    return 0;
}
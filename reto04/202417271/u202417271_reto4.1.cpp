#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main() {
    srand(time(0));

    int cantidadNumeros = rand() % 401 + 100;

    cout << "Cantidad de numeros aleatorios a mostrar: " << cantidadNumeros << endl;
    cout << "Numeros aleatorios generados:" << endl;

    for (int i = 0; i < cantidadNumeros; ++i) {
        int numero = rand() % 10000 + 1;
        cout << numero << " ";
    }
    cout << endl;

    system("pause");

    return 0;
}

#include <iostream>
#include <cstdlib> 
#include <ctime>   
using namespace std;

int* generarArregloAleatorio(int& tamano) {
    
    tamano = rand() % 401 + 100;  

   
    int* arreglo = new int[tamano];

    for (int i = 0; i < tamano; ++i) {
        arreglo[i] = rand() % 10000 + 1;  
    }


    return arreglo;
}

int main() {
    
    srand(time(0));

    
    int tamanoArreglo;

    
    int* arreglo = generarArregloAleatorio(tamanoArreglo);

    
    cout << "Tama�o del arreglo generado: " << tamanoArreglo << endl;

    
    cout << "Elementos del arreglo:" << endl;
    for (int i = 0; i < tamanoArreglo; ++i) {
        cout << arreglo[i] << " ";
        if ((i + 1) % 10 == 0) {  
            cout << endl;
        }
    }
    cout << endl;

    delete[] arreglo;

    return 0;
}

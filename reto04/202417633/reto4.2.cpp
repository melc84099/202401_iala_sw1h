#include <iostream>

using namespace std;

int** crearMatriz(int n, int m) {
    int** matrix = new int* [n];

    for (int i = 0; i < n; ++i) {
        matrix[i] = new int[m]();
    }

    return matrix;
}

void imprimirMatriz(int** matrix, int n, int m) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            cout << matrix[i][j] << " ";
        }
        cout << endl;
    }
}

void liberarMatriz(int** matrix, int n) {
    for (int i = 0; i < n; ++i) {
        delete[] matrix[i];
    }

    delete[] matrix;
}

int main() {
    int n, m;

    cout << "Ingrese el numero de filas (n): ";
    cin >> n;

    cout << "Ingrese el numero de columnas (m): ";
    cin >> m;

    int** matriz = crearMatriz(n, m);

    cout << "Matriz inicializada con ceros:" << endl;
    imprimirMatriz(matriz, n, m);

    liberarMatriz(matriz, n);

    return 0;
}

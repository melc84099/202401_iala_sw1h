#include <iostream>

using namespace std;

int** crearMatriz(int n, int m) {
    int** matriz = new int* [n];

    for (int i = 0; i < n; ++i) {
        matriz[i] = new int[m];

        for (int j = 0; j < m; ++j) {
            matriz[i][j] = 0;
        }
    }

    return matriz;
}

void imprimirMatriz(int** matriz, int n, int m) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            cout << matriz[i][j] << " ";
        }
        cout << endl;
    }
}

int main() {
    int filas, columnas;
    cout << "Ingrese el n�mero de filas: ";
    cin >> filas;
    cout << "Ingrese el n�mero de columnas: ";
    cin >> columnas;

    int** matriz = crearMatriz(filas, columnas);

    cout << "Matriz inicializada con ceros:" << endl;
    imprimirMatriz(matriz, filas, columnas);
    
    for (int i = 0; i < filas; ++i) {
        delete[] matriz[i];
    }
    delete[] matriz;

    system("pause");
    return 0;
}
